<?php


/**
 * Implements hook_features_export_options().
 */
function features_all_features_export_options() {
  $options = array();
  foreach (features_get_components() as $key => $info) {
    if ($key != 'features_source' && $key != 'features_override' && !empty($info['feature_source'])) {
      $options[$key] = $key;
    }
  }
  return $options;
}

/**
 * Implements hook_features_export().
 */
function features_all_features_export($data, &$export, $module_name = '') {
  foreach ($data as $name) {
    $export['features']['features_all'][$name] = $name;
    if ($options = features_invoke($name, 'features_export_options')) {
      if ($map = features_get_default_map($name)) {
        foreach ($map as $k => $v) {
          if (isset($options[$k]) && $v !== $module_name) {
            unset($options[$k]);
          }
        }
      }
      if ($options) {
        $pipe[$name] = $options;
      }
    }
  }

  return $pipe;
}

/**
 * Implements hook_features_export_render().
 */
function features_all_features_export_render($module, $data, $export = NULL) {
  $code = array();
  $code[] = '  // This is just to not confuse features ui.';
  $code[] = '  $components = array();';
  $code[] = '';

  foreach ($data as $name) {
    $code[] = "  \$components[$name] = $name;";
    $code[] = "";
  }

  $code[] = '  return $components;';
  $code = implode("\n", $code);
  return array('features_all_defaults' => $code);
}
